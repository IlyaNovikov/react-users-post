import {useMemo} from "react";

export const useSortedPost = (posts, sort) => {
    const sortedPosts = useMemo(() => {
        if (sort) {
            return [...posts].sort((a, b) => a[sort].localeCompare(b[sort]))
        }
        return posts;
    }, [sort, posts])
    return sortedPosts;
}

export const usePost = (posts, sort, query) =>{
    const sortedPost = useSortedPost(posts, sort)
    const searchedAndSortedPosts = useMemo(() => {
        return sortedPost.filter((post) => {
            return post.title.toLowerCase().includes(query.toLowerCase())
        })
    }, [query, sortedPost])
    return searchedAndSortedPosts
}
