import React, {useContext} from 'react';
import {Navigate, Route, Routes} from "react-router-dom";
import {privateRoutes, publicRoutes} from "../../router";
import {AuthContext} from "../../context";


const AppRouter = () => {
    const {isAuth, setIsAuth} = useContext(AuthContext)
    return (
        <div>
            {
                isAuth
                    ? <Routes>
                        {privateRoutes.map((route, index) =>
                            <Route key={index} path={route.path} element={route.component} exact={route.exact}/>
                        )}
                        <Route key={Date.now()} path="*" element={<Navigate to="/posts"/>}/>
                    </Routes>
                    : <Routes>
                        {publicRoutes.map((route, index) =>
                            <Route key={index} path={route.path} element={route.component} exact={route.exact}/>
                        )}
                        <Route key={Date.now()} path="*" element={<Navigate to="/login"/>}/>
                    </Routes>
            }
        </div>
    );
};

export default AppRouter;
