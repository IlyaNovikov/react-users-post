import React, {useEffect, useState} from 'react';
import {useParams} from "react-router-dom";
import PostService from "../../API/PostService";
import Loader from "../UI/loader/loader";
import {useFetching} from "../hooks/useFetching";

const PostIdPage = () => {
    const params = useParams()
    const [post, setPost] = useState({});
    const [comments, setComments] = useState([]);

    const [fetchPostById, isLoading, error] = useFetching(async () => {
        const response = await PostService.getByID(params.id)
        setPost(response.data)
    });

    const [fetchComments, isComLoading, comError] = useFetching(async (id) => {
        const response = await PostService.getCommentsByPostID(params.id)
        setComments(response.data)
    });

    useEffect(() => {
        fetchPostById(params.id);
        fetchComments(params.id)
    }, [])
    return (
        <div>
            <h1>Это страница поста {params.id}</h1>
            {isLoading
                ? <Loader/>
                : <div>{post.id}. {post.title}</div>
            }
            <h1>Комментарии</h1>
            {isComLoading
                ? <Loader/>
                : <div>
                    {comments.map(comm =>
                    <div style={{marginTop: 15}}>
                        <h5>{comm.email}</h5>
                        <h5>{comm.body}</h5>
                    </div>
                    )}
                </div>
            }
        </div>
    );
};

export default PostIdPage;
