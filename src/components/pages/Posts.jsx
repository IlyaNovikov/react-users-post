import React, {useEffect, useRef, useState} from "react";
import {useFetching} from "../hooks/useFetching";
import PostService from "../../API/PostService";
import {getPageCount} from "../utils/pages";
import MyButton from "../UI/button/MyButton";
import MyModal from "../UI/MyModal/MyModal";
import PostForm from "../PostForm/PostForm";
import PostFilter from "../PostFilter/PostFilter";
import PostList from "../PostList/PostList";
import Pagination from "../UI/pagination/Pagination";
import Loader from "../UI/loader/loader";
import {usePost} from "../hooks/usePosts";
import {useObserver} from "../hooks/useObserver";
import MySelect from "../UI/select/MySelect";


function Posts() {
    const [posts, setPosts] = useState([])
    const [filter, setFilter] = useState({sort: '', query: ''});
    const [modal, setModal] = useState(false);
    const [totalPages, setTotalPages] = useState(0);
    const [limit, setLimit] = useState(10);
    const [page, setPage] = useState(1);
    const lastElement = useRef();

    const [fetchPosts, isPostsLoading, postError] = useFetching(async () => {
        const response = await PostService.getAll(limit, page)
        setPosts([...posts, ...response.data])
        const totalCount = response.headers['x-total-count'];
        setTotalPages(getPageCount(totalCount, limit));
    })

    const sortedAndSearchedPost = usePost(posts, filter.sort, filter.query);

    useObserver(lastElement, page < totalPages, isPostsLoading, () => {setPage(page + 1);})

    useEffect(() => {
        fetchPosts();
    }, [page])


    const createPost = (newPost) => {
        setPosts([...posts, newPost])
        setModal(false)
    }

    const removePost = (post) => {
        setPosts(posts.filter(p => p.id !== post.id))
    }

    const changePage = (page) => {
        setPage(page)
    }

    return (
        <div className="App">
            <MyButton style={{marginTop: '30px'}} onClick={() => setModal(true)}>
                Создать новый пост
            </MyButton>
            <MyModal visible={modal} setVisible={setModal}>
                <PostForm create={createPost}/>
            </MyModal>
            <hr style={{margin: '15px 0'}}/>
            <PostFilter filter={filter} setFilter={setFilter}/>
            <MySelect
            value={limit}
            onChange={value => setLimit(value)}
            defaultValue="Количество элементов на странице"
            options={[
                {value: 5, name: '5'},
                {value: 10, name: '10'},
                {value: 15, name: '15'},
                {value: 25, name: '25'},
                {value: -1, name: 'показать всё'},
            ]}
            />
            {postError &&
            <h1>Произошла ошибка ${postError}</h1>
            }
            <PostList remove={removePost} posts={sortedAndSearchedPost} title='Список Постов № 1'/>
            <div ref={lastElement} style={{height: 20, background: 'red'}}></div>
            {isPostsLoading &&
            <div style={{display: 'flex', justifyContent: 'center', marginTop: '50px'}}><Loader/></div>
            }
            <Pagination page={page} changePage={changePage} totalPages={totalPages}/>
        </div>
    );
}

export default Posts;
