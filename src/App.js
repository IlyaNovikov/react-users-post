import React, {useEffect, useState} from "react";
import './styles/App.css'
import {BrowserRouter} from "react-router-dom";
import AppRouter from "./components/AppRouter/AppRouter";
import Navbar from "./components/UI/Navbar/Navbar";
import {AuthContext} from "./context";


function App() {
    const [isAuth, setIsAuth] = useState(false)
    useEffect(() => {
        if(localStorage.getItem('auth')){
            setIsAuth(true)
        } else {
            setIsAuth(false)
        }
    }, [])
    return (
        <AuthContext.Provider value={{
            isAuth,
            setIsAuth
        }}>
            <BrowserRouter>
                <Navbar/>
                <AppRouter/>
            </BrowserRouter>
        </AuthContext.Provider>
    )
}

export default App;
