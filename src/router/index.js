import About from "../components/pages/About";
import Posts from "../components/pages/Posts";
import PostIdPage from "../components/pages/PostIdPage";
import Error from "../components/pages/Error";
import Login from "../components/pages/login";

export const privateRoutes = [
    {path: '/about', component: <About/>, exact: true},
    {path: '/posts', component: <Posts/>, exact: true},
    {path: '/posts/:id', component: <PostIdPage/>, exact: true},
    {path: '/error', component: <Error/>, exact: true},
]

export const publicRoutes = [
    {path: '/login', component: <Login/>, exact: true},

]
